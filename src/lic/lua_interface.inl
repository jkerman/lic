//
// lua_interface.inl - Templated class that handles most of the interface responsibilities
// Copyright 2012 Matt Modaff
//
// See the LICENSE file for license details
//

#ifndef LIC_LUA_INTERFACE_INL
#define LIC_LUA_INTERFACE_INL

#include "lua_interface.h"
#include "lua_interface_base.inl"
#include "lua_interface_default.inl"
#include "lua_interface_pointer.inl"
#include "lua_interface_reference.inl"

#endif
