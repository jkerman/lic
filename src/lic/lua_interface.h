//
// lua_interface.h - Templated class that handles most of the interface responsibilities
// Copyright 2012 Matt Modaff
//
// See the LICENSE file for license details
//

#ifndef LIC_LUA_INTERFACE_H
#define LIC_LUA_INTERFACE_H

#include "lua_interface_default.h"
#include "lua_interface_pointer.h"
#include "lua_interface_reference.h"
#include "lua_interface_number.h"
#include "lua_interface_string.h"

#endif
